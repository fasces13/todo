DROP DATABASE IF EXISTS todo;
CREATE DATABASE todo;
USE todo;

Create table user (
    id int primary key auto_increment,
    username varchar(30) not null unique,
    password varchar(255) not null,
    firstname varchar(100),
    lastname varchar(100)
);
CREATE TABLE task (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    added timestamp DEFAULT CURRENT_TIMESTAMP,
    description text,
    user_id int not null,
    index (user_id),
    foreign key (user_id) references user(id)
    on delete restrict
)
